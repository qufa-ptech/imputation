# QUFA Imputation & Remove outlier

> `QUFA - 결측치 보완 및 이상치 제거`

## 1. Environment

- python 3.9.4

## 2. Installation

### Install the packages

```sh
> pip3 install -r requirements.txt
```

### Docker Deploy

- docker build -t qufa/qufa-imputation .
- env-sample 참고하여 env 생성
- docker run -p 6000:5000 --rm -d --name qufa-imputation --env-file ./env -t qufa/qufa-imputation
