from flask import Flask, jsonify, request, abort
from threading import Thread
from .threadedTask.task import threaded_imputation_task, threaded_outlier_task

app = Flask(__name__)


@app.before_request
def check_api():
    request.get_json(force=True)
    if 'Authorization' in request.headers:
        token = request.headers['Authorization']
        if token != 'ptech-token':
            abort(401)
    else:
        abort(401)


@app.route("/impute", methods=['POST'])
def impute():
    filename = request.json['file_name']
    thread = Thread(target=threaded_imputation_task, args=([filename]))
    thread.daemon = True
    thread.start()
    return jsonify(
        {'started': True}
    )


@app.route("/outlier", methods=['POST'])
def outlier():
    filename = request.json['file_name']
    thread = Thread(target=threaded_outlier_task, args=([filename]))
    thread.daemon = True
    thread.start()
    return jsonify(
        {'started': True}
    )
